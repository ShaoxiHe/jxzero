package cn.net.gdjx.jxzero.exception;

public class JxServerException extends RuntimeException {

    private String clientMsg;

    public JxServerException(String message, String clientMsg) {
        super(message);
        this.clientMsg = clientMsg;
    }

    public String getClientMsg() {
        return this.clientMsg;
    }
}
