package cn.net.gdjx.jxzero.exception;

public class NotFoundException extends RuntimeException {

    private String clientMsg;

    public NotFoundException(String message, String clientMsg) {
        super(message);
        this.clientMsg = clientMsg;
    }

    public String getClientMsg() {
        return this.clientMsg;
    }
}
