package cn.net.gdjx.jxzero.config;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class WePayService {

    private WePayConfig wePayConfig;

    @Autowired
    public WePayService(WePayConfig wePayConfig) {
        this.wePayConfig = wePayConfig;
    }

    @Bean
    public WXPay wxPay() {
        return new WXPay(wePayConfig, WXPayConstants.SignType.MD5, true);
//        return new WXPay(wePayConfig, WXPayConstants.SignType.MD5);
    }
}