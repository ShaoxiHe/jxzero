package cn.net.gdjx.jxzero.config;

import com.github.wxpay.sdk.WXPayConfig;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class WePayConfig implements WXPayConfig {

    private byte[] certData;

    public WePayConfig() throws IOException {
        String certPath = "fakepath";
        File certFile = new File(certPath);
        InputStream certStream = new FileInputStream(certFile);
        this.certData = new byte[(int) certFile.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    @Override
    public String getAppID() {
        return "fakeid";
    }

    @Override
    public String getMchID() {
        return "fakeid";
    }

    @Override
    public String getKey() {
        return "fakekey";
    }

    @Override
    public InputStream getCertStream() {
        return new ByteArrayInputStream(certData);
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 10000;
    }
}
