package cn.net.gdjx.jxzero.repo;

import cn.net.gdjx.jxzero.model.refund.RefundTransaction;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RefundTransactionRepo extends MongoRepository<RefundTransaction, String> {

}
