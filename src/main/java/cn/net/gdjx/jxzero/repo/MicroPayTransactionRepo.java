package cn.net.gdjx.jxzero.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.net.gdjx.jxzero.model.micropay.MicroPayTransaction;

public interface MicroPayTransactionRepo extends MongoRepository<MicroPayTransaction, String> {

    MicroPayTransaction findByOutTradeNo(String outTradeNo);
}