package cn.net.gdjx.jxzero.service;

import cn.net.gdjx.jxzero.form.RefundForm;
import cn.net.gdjx.jxzero.model.refund.RefundTransaction;
import cn.net.gdjx.jxzero.model.wechatpay.Coupon;
import cn.net.gdjx.jxzero.repo.RefundTransactionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// TODO: 接口化
@Service
public class RefundTransactionService {

    private RefundTransactionRepo refundTransactionRepo;

    @Autowired
    public RefundTransactionService(RefundTransactionRepo refundTransactionRepo) {
        this.refundTransactionRepo = refundTransactionRepo;
    }

    public RefundTransaction save(RefundTransaction refundTransaction) {
        return refundTransactionRepo.save(refundTransaction);
    }

    private void processResult(RefundTransaction refundTransaction, Map<String, String> resultData, RefundForm refundForm) {
        refundTransaction.setOutTradeNo(resultData.get("out_trade_no"));
        refundTransaction.setOutRefundNo(resultData.get("out_refund_no"));
        refundTransaction.setRefundId(resultData.get("refund_id"));
        refundTransaction.setRefundFee(Integer.parseInt(resultData.get("refund_fee")));
        refundTransaction.setSettlementRefundFee(Integer.parseInt(resultData.get("settlement_refund_fee")));
        refundTransaction.setTotalFee(Integer.parseInt(resultData.get("total_fee")));
        refundTransaction.setSettlementTotalFee(Integer.parseInt(resultData.get("settlement_total_fee")));
        refundTransaction.setFeeType(resultData.get("fee_type"));
        refundTransaction.setCashFee(Integer.parseInt(resultData.get("cash_fee")));
        refundTransaction.setCashFeeType(resultData.get("cash_fee_type"));

        //
        refundTransaction.setOutTradeNo(refundForm.getOutTradeNo());
        refundTransaction.setOutRefundNo(refundForm.getOutRefundNo());
        refundTransaction.setRefundFeeType(refundForm.getRefundFeeType());
        refundTransaction.setRefundDesc(refundForm.getRefundDesc());

        processCoupons(refundTransaction, resultData);
    }

    private void processCoupons(RefundTransaction refundTrans, Map<String, String> resultData) {
        List<Coupon> couponList = new ArrayList<>();
        if (null != resultData.get("coupon_refund_count")) {
            Integer couponCount = Integer.parseInt(resultData.get("coupon_refund_count"));

            for (int i = 0; i < couponCount; i++) {
                String typeTag = "coupon_type_" + Integer.toString(i);
                String idTag = "coupon_refund_id_" + Integer.toString(i);
                String feeTag = "coupon_refund_fee_" + Integer.toString(i);

                Coupon coupon = new Coupon();
                coupon.setCouponId(resultData.get(idTag));
                coupon.setFee(Integer.parseInt(resultData.get(feeTag))); // 其实这里应该不可能抛错误的
                coupon.setType(resultData.get(typeTag));

                couponList.add(coupon);
            }
        }

        refundTrans.setRefundCouponList(couponList);
    }

    public void processAndSave(RefundTransaction rt, Map<String, String> resultData, RefundForm refundForm) {
        processResult(rt, resultData, refundForm);
        save(rt);
    }
}
