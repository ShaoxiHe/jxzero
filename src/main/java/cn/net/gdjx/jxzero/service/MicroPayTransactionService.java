package cn.net.gdjx.jxzero.service;

import cn.net.gdjx.jxzero.exception.JxServerException;
import cn.net.gdjx.jxzero.form.MicroPayForm;
import cn.net.gdjx.jxzero.model.micropay.MicroPayTransaction;
import cn.net.gdjx.jxzero.model.micropay.ResultDetail;
import cn.net.gdjx.jxzero.model.micropay.TradeState;
import cn.net.gdjx.jxzero.model.wechatpay.Coupon;
import cn.net.gdjx.jxzero.model.wechatpay.Result;
import cn.net.gdjx.jxzero.repo.MicroPayTransactionRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MicroPayTransactionService {

    /**
     * 刷卡支付订单仓库
     */
    private MicroPayTransactionRepo mptRepo;

    public MicroPayTransactionService(MicroPayTransactionRepo mptRepo) {
        this.mptRepo = mptRepo;
    }

    /**
     * 通过内部订单号查找
     * @param outTradeNo 内部订单号
     * @return 刷卡支付订单
     */
    public MicroPayTransaction findByOutTradeNo(String outTradeNo) {
        return mptRepo.findByOutTradeNo(outTradeNo);
    }

    /**
     * 通过微信订单号查找
     * @param transactionId 微信订单号
     * @return 刷卡支付订单
     */
    public MicroPayTransaction findByTransactionId(String transactionId) {
        List<MicroPayTransaction> mptList = mptRepo.findAll();

        for (MicroPayTransaction mpt : mptList) {
            if (null == mpt.getResultDetail()) {
                continue;
            }

            if (mpt.getResultDetail().getTransactionId().equals(transactionId)) {
                return mpt;
            }
        }

        return null;
    }

    /**
     * 储存订单
     * @param mpt 订单
     * @return 订单
     */
    public MicroPayTransaction save(MicroPayTransaction mpt) {
        return mptRepo.save(mpt);
    }

    /**
     * 合体方法，处理和储存
     * @param mpt
     * @param resultData
     * @param microPayForm
     * @return
     */
    public MicroPayTransaction processAndSave(MicroPayTransaction mpt, Map<String, String> resultData, MicroPayForm microPayForm) {
        processResult(mpt, resultData, microPayForm);
        return save(mpt);
    }

    /**
     * 根据返回数据和表单数据处理刷卡支付订单
     * @param mpt 刷卡支付订单对象
     * @param resultData 返回数据
     * @param microPayForm 刷卡支付表单
     */
    private void processResult(MicroPayTransaction mpt, Map<String, String> resultData, MicroPayForm microPayForm) {
        // mpt.setSceneInfo(); // 这个应该暂时用不上
        Result result = new Result(resultData.get("result_code"), resultData.get("err_code"), resultData.get("err_code_des"));
        mpt.setResult(result);

        if (null != microPayForm) {
            mpt.setAttach(microPayForm.getAttach());
            mpt.setBody(microPayForm.getBody());

            // TODO: 操作日志
            // mpt.setDetail(microPayForm.getDetail());
            // mpt.setSpbillCreateIp(microPayForm.getSpbillCreateIp());
            // mpt.setDeviceInfo(microPayForm.getDeviceInfo());
            if (result.getCode().equals("FAIL")) {
                mpt.setOutTradeNo(microPayForm.getOutTradeNo());
            }
        }


        if (result.getCode().equals("SUCCESS")) {
            mpt.setOutTradeNo(resultData.get("out_trade_no"));
            mpt.setTotalFee(Integer.parseInt(resultData.get("total_fee")));

            ResultDetail resultDetail = mpt.getResultDetail();
            if (null == mpt.getResultDetail()) {
                resultDetail = new ResultDetail();
                mpt.setResultDetail(resultDetail);
            }
            resultDetail.setOpenId(resultData.get("openid"));
            resultDetail.setIsSubscribe(resultData.get("is_subscribe"));
            resultDetail.setTradeType(resultData.get("trade_type"));
            resultDetail.setBankType(resultData.get("bank_type"));
            resultDetail.setFeeType(resultData.get("fee_type"));
            resultDetail.setCashFeeType(resultData.get("cash_fee_type"));
            resultDetail.setTransactionId(resultData.get("transaction_id"));
            resultDetail.setTimeEnd(resultData.get("time_end"));

            resultDetail.setCashFee(Integer.parseInt(resultData.get("cash_fee")));

            if (null != resultData.get("coupon_fee")) {
                resultDetail.setCouponFee(Integer.parseInt(resultData.get("coupon_fee")));
            }

            if (null != resultData.get("settlement_total_fee")) {
                resultDetail.setSettlementTotalFee(Integer.parseInt(resultData.get("settlement_total_fee")));
            }

            // 仅在查询接口返回
            String tradeState = resultData.get("trade_state");
            String tradeStateDesc = resultData.get("trade_state_desc");
            if (null != tradeState && null != tradeStateDesc) {
                resultDetail.setTradeState(new TradeState(tradeState, tradeStateDesc));
            }

            processCoupons(mpt, resultData);
        }
    }

    /**
     * 处理返回数据中的代金券 并更新订单对象
     * @param mpt 订单对象
     * @param resultData 返回数据
     */
    private void processCoupons(MicroPayTransaction mpt, Map<String, String> resultData) {
        Integer couponCount;

        try {
            if (null == resultData.get("coupon_count")) {
                return;
            }
            couponCount = Integer.parseInt(resultData.get("coupon_count"));
        } catch (NumberFormatException e) {
            throw new JxServerException("转换coupon_count的时候发生未知错误，" + resultData.get("coupon_count"), "发生了未知错误");
        }

        List<Coupon> couponList = new ArrayList<>();

        for (int i = 0; i < couponCount; i++) {
            String typeTag = "coupon_type_" + Integer.toString(i);
            String idTag = "coupon_id_" + Integer.toString(i);
            String feeTag = "coupon_fee_" + Integer.toString(i);

            Coupon coupon = new Coupon();
            coupon.setCouponId(resultData.get(idTag));
            System.out.println(resultData.get(feeTag));
            coupon.setFee(Integer.parseInt(resultData.get(feeTag))); // 其实这里应该不可能抛错误的
            coupon.setType(resultData.get(typeTag));

            couponList.add(coupon);
        }

        mpt.setCouponList(couponList);
    }

    public List<MicroPayTransaction> findAll() {
        return mptRepo.findAll();
    }
}
