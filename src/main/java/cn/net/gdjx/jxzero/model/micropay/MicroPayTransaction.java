package cn.net.gdjx.jxzero.model.micropay;

import cn.net.gdjx.jxzero.model.wechatpay.Coupon;
import cn.net.gdjx.jxzero.model.wechatpay.Result;
import org.springframework.data.annotation.Id;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MicroPayTransaction {

    @Id
    private String id;

    private String attach;
    private String body;

    // TODO: Detail Class
//    private String detail;

    private String spbillCreateIp;
    private String deviceInfo;

    private Integer totalFee;

    // TODO: SceneInfo class
    private String sceneInfo;

    private String outTradeNo;
    
    private Result result;

    private ResultDetail resultDetail;

    private List<Coupon> couponList;

    private boolean reversed;

    public MicroPayTransaction() {
        this.couponList = new ArrayList<>();
    }
}