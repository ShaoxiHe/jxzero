package cn.net.gdjx.jxzero.model.refund;

import cn.net.gdjx.jxzero.model.wechatpay.Coupon;
import cn.net.gdjx.jxzero.model.wechatpay.Result;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@Data
public class RefundTransaction {

    @Id
    private String id;

    private String outTradeNo;
    private String outRefundNo;
    private String refundId;
    private Integer refundFee;
    private Integer settlementRefundFee;

    private Integer totalFee;
    private Integer settlementTotalFee;

    private String feeType;
    private Integer cashFee;
    private String cashFeeType;
    private Integer cashRefundFee;
    private Integer couponRefundFee;

    //------ 从退款表单获取来的
    private String refundFeeType;
    private String refundDesc;
    private String refundAccount;


    private List<Coupon> refundCouponList;

    private Result result;

    public RefundTransaction() {
        this.refundCouponList = new ArrayList<>();
    }
}
