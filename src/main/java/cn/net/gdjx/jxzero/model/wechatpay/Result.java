package cn.net.gdjx.jxzero.model.wechatpay;

import lombok.Data;

@Data
public class Result {
    private String code;
    private String errCode;
    private String errCodeDes;

    public Result(String code, String errCode, String errCodeDes) {
        this.code = code;
        this.errCode = errCode;
        this.errCodeDes = errCodeDes;
    }
}