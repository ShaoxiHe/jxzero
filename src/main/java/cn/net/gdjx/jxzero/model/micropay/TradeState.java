package cn.net.gdjx.jxzero.model.micropay;

import lombok.Data;

@Data
public class TradeState {
    private String code;
    private String desc;

    public TradeState() {

    }

    public TradeState(String code, String trade_state_desc) {
        this.code = code;
        this.desc = trade_state_desc;
    }
}