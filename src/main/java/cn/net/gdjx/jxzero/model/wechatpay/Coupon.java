package cn.net.gdjx.jxzero.model.wechatpay;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Coupon {

    private Integer fee;

    private String type;

    private String couponId;

    public Coupon() {

    }

    public Coupon(String couponId, String type, Integer fee) {
        this.couponId = couponId;
        this.type = type;
        this.fee = fee;
    }
}