package cn.net.gdjx.jxzero.model.micropay;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ResultDetail {
    private String openId;
    private String isSubscribe;
    private String tradeType;
    private String bankType;
    private String feeType;
    private Integer settlementTotalFee;
    private Integer couponFee;
    private String cashFeeType;
    private Integer cashFee;
    private String transactionId;
    private String timeEnd;

    // TODO: promotionDetail class
    private String promotionDetail;

    private TradeState tradeState;

    public ResultDetail() {

    }
}