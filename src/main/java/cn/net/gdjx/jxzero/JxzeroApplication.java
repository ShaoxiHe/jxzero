package cn.net.gdjx.jxzero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxzeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(JxzeroApplication.class, args);
	}
}
