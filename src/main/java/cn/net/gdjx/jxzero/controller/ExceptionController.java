package cn.net.gdjx.jxzero.controller;

import cn.net.gdjx.jxzero.exception.JxServerException;
import cn.net.gdjx.jxzero.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Slf4j
public class ExceptionController {

    @ExceptionHandler(JxServerException.class)
    public ModelAndView handle500(HttpServletRequest req, JxServerException e) {
        log.error("【系统错误】{}, 请求: {}", e.getMessage(), req.getRequestURL().toString());
        Map<String, String> map = new HashMap<>();
        map.put("errorMsg", e.getClientMsg());
        return new ModelAndView("error/500", map);
    }

    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handle404(HttpServletRequest req, NotFoundException e) {
        log.error("【404】{}, 请求: {}", e.getMessage(), req.getRequestURL().toString());
        Map<String, String> map = new HashMap<>();
        map.put("errorMsg", e.getClientMsg());
        return new ModelAndView("error/404", map);
    }
}
