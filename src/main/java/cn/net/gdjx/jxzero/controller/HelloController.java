package cn.net.gdjx.jxzero.controller;

import cn.net.gdjx.jxzero.config.WePayConfig;
import cn.net.gdjx.jxzero.model.micropay.MicroPayTransaction;
import cn.net.gdjx.jxzero.model.micropay.ResultDetail;
import cn.net.gdjx.jxzero.repo.MicroPayTransactionRepo;

import com.github.wxpay.sdk.WXPayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/play")
public class HelloController {

    @Autowired
    private WePayConfig wePayConfig;

    @Autowired
    private MicroPayTransactionRepo mptRepo;

    @GetMapping("/hello")
    public String hello() {
        return "hello, world!";
    }

    @GetMapping("/hello2")
    public String hello2() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/xml");
        HttpEntity<String> requestEntity = new HttpEntity<>("<xml><message>hello</message></xml>", headers);
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:8080/mock/micropay", HttpMethod.POST, requestEntity, String.class);
        System.out.println(result.getBody());
        return result.getBody();
    }

    @GetMapping("/getsandboxsign")
    public String getSandboxSign() {
        Map<String, String> sandboxData = new HashMap<>();
        sandboxData.put("mch_id", wePayConfig.getMchID());
        sandboxData.put("nonce_str", WXPayUtil.generateNonceStr());
        String xmlData;
        try {
            sandboxData.put("sign", WXPayUtil.generateSignature(sandboxData, wePayConfig.getKey()));
            xmlData = WXPayUtil.mapToXml(sandboxData);
            System.out.println(xmlData);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }

        String url = "https://api.mch.weixin.qq.com/sandboxnew/pay/getsignkey";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/xml");
        HttpEntity<String> requestEntity = new HttpEntity<>(xmlData, headers);
        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        System.out.println(result);
        return result.getBody();
    }

    @GetMapping("/insert")
    public void insert() {
        MicroPayTransaction mpt = new MicroPayTransaction();

        ResultDetail resultDetail = mpt.getResultDetail();
        if (null == resultDetail) {
            resultDetail = new ResultDetail();
            mpt.setResultDetail(resultDetail);
        }
        resultDetail.setCashFee(444);
        // mpt.getCoupons().add(new Coupon("1", "mua1"));
        // mpt.getCoupons().add(new Coupon("2", "mua2"));
        // mpt.getCoupons().add(new Coupon("3", "mua3"));
//        mpt.setResultDetail(resultDetail);
        mptRepo.save(mpt);
    }
}