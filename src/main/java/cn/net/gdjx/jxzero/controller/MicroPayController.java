package cn.net.gdjx.jxzero.controller;

import cn.net.gdjx.jxzero.exception.JxServerException;
import cn.net.gdjx.jxzero.exception.NotFoundException;
import cn.net.gdjx.jxzero.model.micropay.*;
import cn.net.gdjx.jxzero.model.wechatpay.Result;
import cn.net.gdjx.jxzero.service.MicroPayTransactionService;
import com.github.wxpay.sdk.WXPay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import cn.net.gdjx.jxzero.form.MicroPayForm;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/micropay")
@Slf4j
public class MicroPayController {

    private WXPay wxPay;
    private MicroPayTransactionService mptService;

    @Autowired
    public MicroPayController(WXPay wxPay, MicroPayTransactionService mptService) {
        this.wxPay = wxPay;
        this.mptService = mptService;
    }


    @GetMapping("/")
    public String index(Model model) {

        List<MicroPayTransaction> mptList = mptService.findAll();

        model.addAttribute("mptList", mptList);

        return "micropay/index";
    }

    @GetMapping("/create")
    public String create(MicroPayForm microPayForm) {
        return "micropay/create";
    }

    @PostMapping("/create")
    public String create(@Valid MicroPayForm microPayForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "micropay/create";
        }
        Map<String, String> microPayData = new HashMap<>();
        microPayData.put("body", microPayForm.getBody());
        microPayData.put("out_trade_no", microPayForm.getOutTradeNo());
        microPayData.put("attach", microPayForm.getAttach());
        microPayData.put("total_fee", Integer.toString(microPayForm.getTotalFee()));
        microPayData.put("auth_code", microPayForm.getAuthCode());
        Map<String, String> resultData;
        try {
            resultData = wxPay.microPay(microPayData);
        } catch (Exception e) {
            throw new JxServerException("【微信SDK异常】" + e.getMessage(), "刷卡支付接口调用失败");
        }

        String returnCode = resultData.get("return_code");

        //------ 订单结果存在两种情况
        //------ 1. 直接失败, 返回错误到用户界面 ------
        if (returnCode.equals("FAIL")) {
            throw new JxServerException(resultData.get("return_msg"), "支付接口调用失败");
        }

        //------ 2. 业务直接成功, 直接存东西
        //====== 3. 调用成功，但业务失败的 ======
        //------    a. USERPAYING, 用户需要输入密码 ------
        //------    b. 其他错误情况
        MicroPayTransaction mpt = new MicroPayTransaction();
        mptService.processAndSave(mpt, resultData, microPayForm);

        return "redirect:/micropay/detail/" + mpt.getOutTradeNo();

        // TODO: 否则，循环查询订单，若依然失败，撤销
        // TODO: 从前端调用API? 从后端嵌入ajax到前端？
    }

    @GetMapping("/detail/{outTradeNo}")
    public String detail(@PathVariable String outTradeNo, Model model) {
        MicroPayTransaction mpt = mptService.findByOutTradeNo(outTradeNo);
        if (null == mpt) {
            throw new NotFoundException("找不到该订单：" + outTradeNo, "找不到该订单");
        }

        model.addAttribute("mpt", mpt);
        return "micropay/detail";
    }

    @GetMapping("/query")
    public String query(@RequestParam String outTradeNo, Model model) {
        // TODO: 利用ajax更新详情页面

        // TODO: 用service 类增加根据transactionId 查找

        MicroPayTransaction mpt = mptService.findByOutTradeNo(outTradeNo);
        if (null == mpt) {
            log.error("尝试查询不在数据库中的订单号" + outTradeNo);
            mpt = new MicroPayTransaction();
        }

        Map<String, String> queryData = new HashMap<>();

        if (null != outTradeNo) {
            queryData.put("out_trade_no", outTradeNo);
        }

        Map<String, String> queryResult;
        try {
            queryResult = wxPay.orderQuery(queryData);
            System.out.println(queryResult);
        } catch (Exception e) {
            throw new JxServerException("【微信SDK异常】" + e.getMessage(), "查询接口调用失败");
        }

        //------ 调用失败
        if (queryResult.get("return_code").equals("FAIL")) {
            throw new JxServerException(queryData.get("return_msg"), "查询接口调用失败");
        }

        // 更新数据库里订单数据
        mptService.processAndSave(mpt, queryResult, null);

        model.addAttribute("mpt", mpt);

        return "micropay/detail";
    }

    // TODO: 做成API
    @PostMapping("/reverse/{outTradeNo}")
    public String reverse(@PathVariable String outTradeNo, Model model) {
        Map<String, String> data = new HashMap<>();
        data.put("out_trade_no", outTradeNo);

        Map<String, String> resultData;
        try {
            resultData = wxPay.reverse(data);
        } catch (Exception e) {
            throw new JxServerException("【微信SDK】" + e.getMessage(), "撤销订单接口调用失败");
        }

        if (resultData.get("return_code").equals("FAIL")) {
            throw new JxServerException("撤销调用失败: " + resultData.get("result_msg"), "撤销订单接口调用失败");
        }

        if (resultData.get("result_code").equals("SUCCESS")) {
            MicroPayTransaction mpt = mptService.findByOutTradeNo(outTradeNo);

            if (null != mpt) {
                mpt.setReversed(true);
                mptService.save(mpt);
            }
        }

        model.addAttribute("recall", resultData.get("recall"));

        Result result = new Result(
                resultData.get("result_code"),
                resultData.get("err_code"),
                resultData.get("err_code_desc")
        );

        model.addAttribute("result", result);

        return "micropay/reverseResult";
    }

    @GetMapping("/downloadbill")
    public String downloadbill() {
        Map<String, String> data = new HashMap<>();

        data.put("bill_date", "20171023");
        data.put("bill_type", "ALL");

        Map<String, String> resultData = null;
        try {
            resultData = wxPay.downloadBill(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assert resultData != null;
        System.out.println(resultData.get("data"));

        return "index";
    }
}