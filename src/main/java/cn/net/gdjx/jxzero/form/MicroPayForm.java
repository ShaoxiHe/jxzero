package cn.net.gdjx.jxzero.form;

import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class MicroPayForm {
    @NotEmpty(message = "请填写商品描述")
    @Size(min = 2, max = 128, message = "商品描述长度为 2-128 个字")
    public String body;

    @NotEmpty(message = "请填写订单号")
    @Size(max = 32, message = "订单号长度最多32位")
    public String outTradeNo;

    @Size(max = 127, message = "说明信息长度不得超出127个字")
    public String attach;

    @NotNull(message = "请填写金额")
    @Min(value = 1, message = "金额单位为分，必须大于1")
    public Integer totalFee;

    @NotEmpty(message = "请输入授权码")
    @Size(max = 18, min = 18, message = "授权码长度必须为18位")
    public String authCode;

    public String toString() {
        return String.format("商品: %s, 订单号: %s, 附加描述: %s, 金额: %d, 授权码: %s",
            this.body, this.outTradeNo, this.attach, this.totalFee, this.authCode);
    }
}