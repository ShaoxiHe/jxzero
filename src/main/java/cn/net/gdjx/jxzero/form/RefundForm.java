package cn.net.gdjx.jxzero.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RefundForm {

    @NotEmpty
    @Size(max = 32, message = "订单号长度最多32位")
    private String outTradeNo;

    @NotEmpty
    @Size(max = 64, message = "退款单号长度最多64位")
    private String outRefundNo;

    @NotNull
    @Min(1)
    private Integer totalFee;

    @NotNull
    @Min(1)
    private Integer refundFee;

    @Size(max = 80, message = "退款原因不能超过80个字")
    private String refundDesc;

    private String refundFeeType;
}
